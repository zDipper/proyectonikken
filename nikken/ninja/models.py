from django.db import models

class Asesor(models.Model):
    DNI = models.CharField(max_length=8, verbose_name="D.N.I.",primary_key=True)
    nombre = models.CharField(max_length=50,verbose_name="Nombres")
    apellido = models.CharField(max_length=50,verbose_name="Apellidos")
    sexo = models.CharField(max_length=1,verbose_name="Sexo")
    fechaNacim = models.DateField(auto_now=False, auto_now_add=False,verbose_name="FechaNaciminento")
    DNImanager = models.ForeignKey('self',null=True ,verbose_name="FK_Asesor_Asesor", on_delete=models.CASCADE)
    class Meta:
        verbose_name = "Asesor"
        verbose_name_plural = "Asesores"
    def __str__(self):
        return self.nombre

class Producto(models.Model):
    idProd = models.CharField(max_length=6, verbose_name="producto_id",primary_key=True)
    descripcion = models.CharField(max_length=45,verbose_name="descripcion_producto")
    precioMayor = models.DecimalField( max_digits=10, decimal_places=2,verbose_name="precioMayor")
    precioVenta = models.DecimalField( max_digits=10, decimal_places=2,verbose_name="precioVenta")
    puntos = models.IntegerField(verbose_name="puntos")
    vc = models.IntegerField(verbose_name="vc")
    ginMediata = models.DecimalField( max_digits=10, decimal_places=2,verbose_name="ginMediata")
    bKinya = models.DecimalField( max_digits=10, decimal_places=2,verbose_name="bKinya")
    bKinyam1 = models.DecimalField( max_digits=10, decimal_places=2,verbose_name="bKinyam1")
    bKinyam2 = models.DecimalField( max_digits=10, decimal_places=2,verbose_name="bKinyam2")
    stock = models.IntegerField(verbose_name="stock")
    class Meta:
        verbose_name = "Producto"
        verbose_name_plural = "Productos"
    def __str__(self):
        return self.descripcion

class Compra(models.Model):
    nrDoc = models.AutoField(verbose_name="nrDoc_Compra",primary_key=True)
    dni = models.ForeignKey(Asesor, verbose_name="dni_Asesor", on_delete=models.CASCADE)
    fecha = models.DateField( auto_now=False,auto_now_add=False,verbose_name="fecha_Compra")
    direccion = models.CharField( max_length=45,verbose_name="direccion")
    class Meta:
        verbose_name = "Compra"
        verbose_name_plural = "Compras"
    def int(self):
        return self.nrDoc


class CompraDetalle(models.Model):
    nrDoc = models.ForeignKey(Compra, verbose_name="nrDoc_Compra_FK_PK", on_delete=models.CASCADE)
    idProd = models.ForeignKey(Producto, verbose_name="nrDoc_Compra_FK_PK", on_delete=models.CASCADE)
    cantidad = models.IntegerField(verbose_name="cantidad_Detalle")
    precio = models.DecimalField( max_digits=10, decimal_places=2,verbose_name="precio_Detalle")
    class Meta:
        verbose_name = "CompraDetalle"
        verbose_name_plural = "CompraDetalles"

class Comision(models.Model):
    IdComision = models.CharField(max_length=10,verbose_name="id Comision",primary_key=True)
    titulo = models.CharField(max_length=50,verbose_name="tutulo")
    class Meta:
        verbose_name = "Comision"
        verbose_name_plural = "Comisiones"
    def __str__(self):
        return self.IdComision





# Create your models here.
