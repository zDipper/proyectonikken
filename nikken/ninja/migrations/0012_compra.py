# Generated by Django 2.2.7 on 2019-11-22 15:57

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('ninja', '0011_producto'),
    ]

    operations = [
        migrations.CreateModel(
            name='Compra',
            fields=[
                ('nrDoc', models.CharField(max_length=10, primary_key=True, serialize=False, verbose_name='nrDoc_Compra')),
                ('fecha', models.DateField(verbose_name='fecha_Compra')),
                ('direccion', models.CharField(max_length=45, verbose_name='direccion')),
                ('dni', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='ninja.Asesor', verbose_name='dni_Asesor')),
            ],
            options={
                'verbose_name': 'Compra',
                'verbose_name_plural': 'Compras',
            },
        ),
    ]
