# Generated by Django 2.2.7 on 2019-11-21 20:17

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
        ('ninja', '0008_delete_asesor'),
    ]

    operations = [
        migrations.CreateModel(
            name='Asesor',
            fields=[
                ('DNI', models.CharField(max_length=8, primary_key=True, serialize=False, verbose_name='D.N.I.')),
                ('nombre', models.CharField(max_length=50, verbose_name='Nombres')),
                ('apellido', models.CharField(max_length=50, verbose_name='Apellidos')),
                ('sexo', models.CharField(max_length=1, verbose_name='Sexo')),
                ('fechaNacim', models.DateField(verbose_name='FechaNaciminento')),
            ],
            options={
                'verbose_name': 'Asesor',
                'verbose_name_plural': 'Asesores',
            },
        ),
    ]
