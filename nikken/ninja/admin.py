from django.contrib import admin
from .models import Asesor
from .models import Producto
from .models import Compra
from .models import CompraDetalle
from .models import Comision
admin.site.register(Asesor)
admin.site.register(Producto)
admin.site.register(Compra)
admin.site.register(CompraDetalle)
admin.site.register(Comision)


# Register your models here.
