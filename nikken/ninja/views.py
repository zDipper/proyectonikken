from django.shortcuts import render, HttpResponse
from .models import Asesor
from .models import Producto
from .models import Compra
from .models import CompraDetalle
from .models import Comision

def asesores(request):
    asesor = Asesor.objects.order_by('nombre')
    return render(request, "ninja/asesores.html", {'asesores': asesor})

def productos(request):
    producto = Producto.objects.order_by('descripcion')
    return render(request, "ninja/productos.html", {'productos': producto})

def compras(request):
    compra = Compra.objects.order_by('nrDoc')
    return render(request, "ninja/compras.html", {'compras': compra})

def compraDetalles(request):
    compraDetalle = CompraDetalle.objects.order_by('id')
    return render(request, "ninja/compraDetalles.html", {'compraDetalles': compraDetalle})

def comisiones(request):
    comision = Comision.objects.order_by('IdComision')
    return render(request, "ninja/compraDetalles.html", {'comisiones': comision})



# Create your views here.
